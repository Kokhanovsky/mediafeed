import { Controller, Get } from "@tsed/common";

@Controller("/status")
export class StatusCtrl {
    @Get()
    status(): string {
        return "ok";
    }
}